# Changelog for Serilog.Enrichers.Sensitive

## 1.2.0

- Add `ShouldMaskMatch` to the `RegexMaskingOperator` to allow implementors to perform further checks on the sensitive value in order to decide whether or not to perform masking

## 1.1.0

- Add support to supply a custom mask value [#6](https://github.com/serilog-contrib/Serilog.Enrichers.Sensitive/issues/6)
- Add support to always mask a property regardless of value [#7](https://github.com/serilog-contrib/Serilog.Enrichers.Sensitive/issues/7)

## 1.0.0

- Removed a number of try/catch blocks that only existed for debugging
- Updated copyright notices
- Bumped version
- Cleaned up README